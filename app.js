const express = require('express'),
      bodyParser = require('body-parser'),
      request = require('request');

var app = express();

app.use(express.static(__dirname + '/files'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use('/', (req, res, next) => {
    
    res.success = data => {
        const result = { status: true };
        if (typeof data !== 'undefined') result.data = data;
        res.send(result);
    };

    res.fail = error => {
        const result = { status: false };
        if (typeof error !== 'undefined') result.error = error;
        res.send(result);
    };
    
    next();
    
});

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/files/html/form.html');
});

app.post('/api/login/', function (req, res) {
    
    const { login, password } = req.body;
    
    var proxiedRequest = request;
    // var proxiedRequest = request.defaults({ 'proxy': 'http://45.77.67.196:3128' });
    
    proxiedRequest.post({
        url: 'https://api.givingassistant.org/ps/login',
        json: {
            username: login,
            password: password,
            _ApplicationId: "RkhLEyTZT4NTJ5PrQTQuxXqNhmetgv1KMMCltHCR",
            _ClientVersion: "js1.9.2",
            _InstallationId: "b6bcdc29-96a2-c8fb-5b60-d88d0c82f1d3",
            _JavaScriptKey: "TQ12gSoYgfEiJHLQRZiPxCSmmfDyKzxCc2BxSZls",
            _method: "GET",
        },
        headers: {
            'Host': 'api.givingassistant.org',
            'Origin': 'https://givingassistant.org',
            'Referer': 'https://givingassistant.org/',
        }
    }, (error, response, body) => {
        
        if (error) return res.fail(error);
        
        if (response.statusCode == 200) {
            res.success();
        } else if (body.error) {
            res.fail(body.error);
        } else {
            res.fail(body);
        }
        
    });
    
});

app.listen(3000, function () {
    console.log('App listening on port 3000');
});
