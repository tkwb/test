$(function() {

    'use strict';
    
    var $form = $('#form'),
        $login = $form.find('[name=login]'),
        $password = $form.find('[name=password]'),
        $result = $('#result');

    $form.submit(function(event) {
        
        event.preventDefault();
        
        var formData = {
            login: $login.val(),
            password: $password.val()
        };
        
        $.post('/api/login/', formData)
            .done(function(data) {
                console.log(data);
                data.status ? success() : fail(data.error);
            })
            .fail(function() {
                fail();
            });
        
    });
    
    function success() {
        $result.show().removeClass('result_error').text('OK');
    }
    
    function fail(error) {
        $result.show().addClass('result_error').text(error || 'Unknown error');
    }

});
